//
//  MoviesTestContainer.swift
//  MovsTests
//
//  Created by André Salla on 21/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import Foundation
@testable import Movs

class MoviesTestContainer: MoviesViewModelDelegate {

    var oldState: MoviesViewState?
    var newState: MoviesViewState?
    var filterUpdatedCalled: Bool = false
    var nextInfo: MovieInfo?

    func stateUpdated(oldState: MoviesViewState, newState: MoviesViewState) {
        self.oldState = oldState
        self.newState = newState
    }

    func filterUpdated() {
        filterUpdatedCalled = true
    }

    func prepareGoNext(info: MovieInfo) {
        nextInfo = info
    }

}
