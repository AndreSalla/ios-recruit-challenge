//
//  MoviesRepositoryTest.swift
//  MovsTests
//
//  Created by André Salla on 21/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import Foundation
import XCTest
@testable import Movs

enum MoviesRepositoryCases {
    case success
    case noData
    case error
}

class MoviesRepositoryTest: MoviesRepositoryProtocol {

    weak var delegate: MoviesRepositoryDelegate?
    var caseTest: MoviesRepositoryCases?
    var moviePerPage: Int = 2

    func obtainMoviesList(at page: Int) {
        guard let caseTest = caseTest else {
            return
        }
        switch caseTest {
        case .success:
            let page = Page<Movie>(
                page: page, totalResults: (page + 1) * moviePerPage,
                totalPages: page + 1, results: generateMovieArray())
            delegate?.list(movies: page)
        case .noData:
            let page = Page<Movie>.init(page: page, totalResults: 0, totalPages: page, results: [])
            delegate?.list(movies: page)
        case .error: delegate?.error(message: "Ocorreu um erro")
        }
    }

    func generateMovieArray() -> [Movie] {
        var movies: [Movie] = []
        for index in 1 ... moviePerPage {
            movies.append(
                Movie(identifier: index,
                      title: "Meu Teste \(index)",
                      posterPath: "/teste.jpg",
                      overview: "Meu teste descrição \(index)",
                      genreIds: [3],
                      releaseDate: "\(2018 - index)-01-01")
            )
        }
        return movies
    }

    func obtainGenresList() {
        guard let caseTest = caseTest else {
            return
        }
        switch caseTest {
        case .success:
            delegate?.list(genre: [
                Genre(identifier: 1, description: "Faroeste"),
                Genre(identifier: 2, description: "Aventura"),
                Genre(identifier: 3, description: "Comédia")
                ])
        case .error:
            delegate?.error(message: "Ocorreu um erro")
        case .noData:
            delegate?.list(genre: [])
        }
    }

    func cleanGenresList() {
    }

    func checkIsFavorite(movie: Movie) -> Bool {
        return movie.identifier == 1
    }

}
