//
//  MovsTests.swift
//  MovsTests
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import XCTest
@testable import Movs

class MoviesViewModelTest: XCTestCase {

    var viewModel: (MoviesViewModelProtocol & MoviesRepositoryDelegate)?
    lazy var repository: MoviesRepositoryTest = MoviesRepositoryTest()

    override func setUp() {
        viewModel = MoviesViewModel(repository: repository)
        repository.delegate = viewModel
        super.setUp()

    }

    override func tearDown() {
        super.tearDown()
    }

    func testListMovies() {
        let container = MoviesTestContainer()
        viewModel?.delegate = container

        repository.caseTest = .success

        guard let viewModel = viewModel else {
            return
        }

        viewModel.obtainMoreMovies()

        XCTAssert(container.newState == MoviesViewState.standing)
        XCTAssert(viewModel.moviesCount == repository.moviePerPage)
        XCTAssert(viewModel.getMovie(at: 0) != nil)
        XCTAssert(viewModel.getMovie(at: repository.moviePerPage) == nil)

    }

    func testListMoviesNoData() {
        let container = MoviesTestContainer()
        viewModel?.delegate = container

        repository.caseTest = .noData

        guard let viewModel = viewModel else {
            return
        }

        viewModel.obtainMoreMovies()

        XCTAssert(container.newState == MoviesViewState.noData)
        XCTAssert(viewModel.moviesCount == 0)
        XCTAssert(viewModel.getMovie(at: 0) == nil)

    }

    func testListMoviesPaginated() {
        let container = MoviesTestContainer()
        viewModel?.delegate = container

        repository.caseTest = .success

        guard let viewModel = viewModel else {
            return
        }

        let numberOfPages = 3

        for _ in 1 ... numberOfPages {
            viewModel.obtainMoreMovies()
        }

        XCTAssert(container.newState == MoviesViewState.standing)
        XCTAssert(viewModel.moviesCount == numberOfPages * repository.moviePerPage)
        XCTAssert(viewModel.getMovie(at: 0) != nil)
        XCTAssert(viewModel.getMovie(at:
            repository.moviePerPage * (numberOfPages - 1)) != nil)
        XCTAssert(viewModel.getMovie(at:
            repository.moviePerPage * numberOfPages) == nil)

    }

    func testFilterMovies() {
        let container = MoviesTestContainer()
        viewModel?.delegate = container

        repository.caseTest = .success

        guard let viewModel = viewModel else {
            return
        }

        viewModel.obtainMoreMovies()
        viewModel.searchItem(text: "1")

        XCTAssert(container.newState == MoviesViewState.standing)
        XCTAssert(viewModel.moviesCount == 1)
        XCTAssert(container.filterUpdatedCalled)
        XCTAssert(viewModel.getMovie(at: 0) != nil)
        XCTAssert(viewModel.getMovie(at: 0)?.title.contains("1") ?? false)

    }

}
