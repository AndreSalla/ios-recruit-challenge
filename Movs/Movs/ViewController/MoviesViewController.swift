//
//  MoviesViewController.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public class MoviesViewController: SearchableController {

    @IBOutlet weak var topViewStoryboard: ExtendedView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchViewConstraint: NSLayoutConstraint?

    private weak var coordinator: MoviesCoordinatorProtocol?

    private var viewModel: MoviesViewModelProtocol?
    private var observer: NSObjectProtocol?

    public init(viewModel: MoviesViewModelProtocol, coordinator: MoviesCoordinatorProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.coordinator = coordinator
        self.tabBarItem = UITabBarItem.init(title: Strings.titleMovies.localized,
                                            image: Images.tabBarItemMovies.image,
                                            tag: 0)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func viewDidLoad() {
        self.topView = topViewStoryboard
        self.topViewConstraint = searchViewConstraint

        super.viewDidLoad()

        configure()

        DispatchQueue.global().async {[weak self] in
            self?.viewModel?.obtainMoreMovies()
        }

    }

    private func configure() {
        self.title = viewModel?.title

        collectionView.register(UINib(nibName: "MovieCollectionViewCell", bundle: nil),
                                forCellWithReuseIdentifier: "collectionCell")

        observer = NotificationCenter.default.addObserver(forName:
            Notification.Name.favoritedNotification, object: nil, queue: nil) { (not) in
                if let info = not.userInfo {
                    let identifier = info["id"] as? Int ?? -1
                    let isFavorite = info["isFavorite"] as? Bool ?? false
                    DispatchQueue.global().async {[weak self] in
                        self?.viewModel?.changeFavorite(identifier: identifier, isFavorite: isFavorite)
                    }
                }
        }
    }

    public override func updateSearchResults(for searchController: UISearchController) {
        if !searchController.isBeingDismissed {
            let text = searchController.searchBar.text
            DispatchQueue.global().async {[weak self] in
                self?.viewModel?.searchItem(text: text ?? "")
            }
        }
    }

    public override func willPresentSearchController(_ searchController: UISearchController) {
        super.willPresentSearchController(searchController)
        searchController.searchBar.text = viewModel?.searchTherm
    }

}

extension MoviesViewController: MoviesViewModelDelegate {

    public func filterUpdated() {
        DispatchQueue.main.async {[weak self] in
            self?.collectionView.reloadData()
        }
    }

    public func stateUpdated(oldState: MoviesViewState, newState: MoviesViewState) {
        DispatchQueue.main.async {[weak self] in
            switch (oldState, newState) {
            case (.none, .loading): self?.loadingState()
            case (_, .error): self?.errorState()
            case (_, .noData): self?.noDataState()
            default: self?.standState()
            }
            self?.collectionView.reloadData()
        }
    }

    private func errorState() {
        let label = UILabel()
        label.text = Strings.unknownError.localized
        label.textAlignment = .center
        collectionView.backgroundView = label
        collectionView.isHidden = false
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }

    private func noDataState() {
        let label = UILabel()
        label.text = Strings.noData.localized
        label.textAlignment = .center
        collectionView.backgroundView = label
        collectionView.isHidden = false
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }

    private func loadingState() {
        collectionView.backgroundView = nil
        collectionView.isHidden = true
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }

    private func standState() {
        collectionView.backgroundView = nil
        collectionView.isHidden = false
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }

    public func prepareGoNext(info: MovieInfo) {
        DispatchQueue.main.async {[weak self] in
            self?.hidesBottomBarWhenPushed = true
            self?.coordinator?.goDetail(
                movie: info.movie, genres: info.genres, isFavorite: info.isFavorite)
            self?.hidesBottomBarWhenPushed = false
        }
    }
}

extension MoviesViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.moviesCount ?? 0
    }

    public func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath)
        (cell as? MovieCollectionViewCell)?.viewModel = viewModel?.getMovie(at: indexPath.row)
        return cell
    }

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {[weak self] in
            self?.viewModel?.selectDetail(at: indexPath.row)
        }
    }
}

extension MoviesViewController: UIScrollViewDelegate {

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y + scrollView.frame.size.height) >
            (scrollView.contentSize.height * 0.85) {
            DispatchQueue.global().async {[weak self] in
                self?.viewModel?.obtainMoreMovies()
            }
        }
    }

}

extension MoviesViewController: UICollectionViewDelegateFlowLayout {

    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let currentWidth = min(collectionView.frame.size.width, collectionView.frame.size.height)
        let width: CGFloat = (currentWidth - 30.0) / 2.0
        return CGSize(width: width,
                      height: width * 1.66)
    }

}
