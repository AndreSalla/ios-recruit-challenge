//
//  FavoritesViewController.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol FavoritesViewControllerProtocol: class {
    func updateFilters(selectedYears: [String], selectedGenres: [Int])
}

public class FavoritesViewController: SearchableController {

    @IBOutlet weak var topViewStoryboard: ExtendedView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchViewConstraint: NSLayoutConstraint?

    private weak var coordinator: FavoritesCoordinatorProtocol?

    private var viewModel: FavoritesViewModelProtocol?
    private var observer: NSObjectProtocol?

    public init(viewModel: FavoritesViewModelProtocol, coordinator: FavoritesCoordinatorProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.coordinator = coordinator
        self.tabBarItem = UITabBarItem.init(title: Strings.titleFavorites.localized,
                                            image: Images.tabBarItemFavorites.image,
                                            tag: 0)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func viewDidLoad() {
        self.topView = topViewStoryboard
        self.topViewConstraint = searchViewConstraint

        super.viewDidLoad()

        configure()

        DispatchQueue.global().async {[weak self] in
            self?.viewModel?.obtainFavorites()
        }

    }

    private func configure() {
        self.title = viewModel?.title

        tableView.register(UINib(nibName: "FavoriteTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "favoriteCell")
        tableView.rowHeight = 150

        observer = NotificationCenter.default.addObserver(forName:
        Notification.Name.favoritedNotification, object: nil, queue: nil) { (not) in
            if let info = not.userInfo,
                !(info["fromFavorites"] as? Bool ?? false) {
                let identifier = info["id"] as? Int ?? -1
                let isFavorite = info["isFavorite"] as? Bool ?? false
                DispatchQueue.global().async {[weak self] in
                    self?.viewModel?.changeFavorite(identifier: identifier, isFavorite: isFavorite, update: true)
                }
            }
        }

        let filterButtom =
            UIBarButtonItem(image: Images.filter.image, style: .plain,
                            target: self, action: #selector(filterPressed(_:)))
        self.navigationItem.rightBarButtonItems = [filterButtom]
    }

    @objc public func filterPressed(_ sender: UIControl) {
        viewModel?.showFilters()
    }

    public override func updateSearchResults(for searchController: UISearchController) {
        if !searchController.isBeingDismissed {
            let text = searchController.searchBar.text
            DispatchQueue.global().async {[weak self] in
                self?.viewModel?.searchItem(text: text ?? "")
            }
        }
    }

    public override func willPresentSearchController(_ searchController: UISearchController) {
        super.willPresentSearchController(searchController)
        searchController.searchBar.text = viewModel?.searchTherm
    }

}

extension FavoritesViewController: FavoritesViewModelDelegate {

    public func unfavorited(at index: Int, identifier: Int) {
        DispatchQueue.main.async {[weak self] in
            self?.tableView.beginUpdates()
            self?.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            self?.tableView.endUpdates()

            NotificationCenter.default.post(
                name: Notification.Name.favoritedNotification,
                object: nil,
                userInfo: ["id": identifier, "isFavorite": false, "fromFavorites": true])
        }
    }

    public func filterUpdated() {
        DispatchQueue.main.async {[weak self] in
            self?.tableView.reloadData()
        }
    }

    public func stateUpdated(newState: FavoritesViewState) {
        DispatchQueue.main.async {[weak self] in
            switch newState {
            case (.standing): self?.standState()
            case (.noData): self?.noDataState()
            }
        }
    }

    private func noDataState() {
        let label = UILabel()
        label.text = Strings.noData.localized
        label.textAlignment = .center
        tableView.backgroundView = label
    }

    private func standState() {
        tableView.backgroundView = nil
    }

    public func modeUpdated(newState: FavoritesViewMode) {
        DispatchQueue.main.async {[weak self] in
            if newState == .filter {

                let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 0, height: 50))
                button.setTitle(Strings.removeFilter.localized, for: .normal)
                button.backgroundColor = UIApplication.style.removeFilterButtonColor
                button.setTitleColor(UIApplication.style.removeFilterTextColor, for: .normal)
                button.titleLabel?.font = UIApplication.style.removeFilterButtomFont
                    if let selfStrong = self {
                        button.addTarget(selfStrong,
                                         action: #selector(selfStrong.removeFilterTapped(_:)),
                                         for: .touchUpInside)
                }

                self?.tableView.tableHeaderView = button
            } else {
                self?.tableView.tableHeaderView = nil
            }
        }
    }

    @objc public func removeFilterTapped(_ sender: UIControl) {
        DispatchQueue.global().async {[weak self] in
            self?.viewModel?.removeFilters()
        }
    }

    public func goToFilters(selectedYear: [String], selectedGenres: [Int]) {
        DispatchQueue.main.async {[weak self] in
            self?.hidesBottomBarWhenPushed = true
            self?.coordinator?.showFilters(selectedYears: selectedYear,
                                     selectedGenres: selectedGenres)
            self?.hidesBottomBarWhenPushed = false
        }
    }

}

extension FavoritesViewController: UITableViewDelegate, UITableViewDataSource {

    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    public func tableView(_ tableView: UITableView,
                          editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return [UITableViewRowAction(style: .destructive, title: Strings.unfavorite.localized) { (_, index) in
            DispatchQueue.global().async {[weak self] in
                self?.viewModel?.unfavorite(at: index.row)
            }
        }]
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.moviesCount ?? 0
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: "favoriteCell") ??
            FavoriteTableViewCell()) as? FavoriteTableViewCell
        cell?.viewModel = viewModel?.getMovie(at: indexPath.row)
        return cell ?? UITableViewCell()
    }

}

extension FavoritesViewController: FavoritesViewControllerProtocol {

    public func updateFilters(selectedYears: [String], selectedGenres: [Int]) {
        DispatchQueue.global().async {[weak self] in
            self?.viewModel?.updateFilters(selectedYears: selectedYears,
                                           selectedGenres: selectedGenres)
        }
    }

}
