//
//  SearchableControllerProtocol.swift
//  Movs
//
//  Created by André Salla on 19/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public class SearchableController: UIViewController {

    weak var topView: UIView?
    var searchController: UISearchController?
    weak var topViewConstraint: NSLayoutConstraint?

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11, *) { } else {
            navigationController?.navigationBar.hideHairlineForSearch()
        }
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if #available(iOS 11, *) { } else {
            navigationController?.navigationBar.showHairlineForSearch()
        }
    }

    private func configure() {
        createSearchController()
    }

    private func createSearchController() {

        if #available(iOS 11, *) {
            self.topView?.isHidden = true
            self.topViewConstraint?.constant = 0

            let search = UISearchController.init(searchResultsController: nil)
            navigationItem.searchController = search
            search.delegate = self
            navigationItem.searchController?.searchResultsUpdater = self
            navigationItem.hidesSearchBarWhenScrolling = false

        } else {
            //Life happens...
            configureSearchBarOlderVersions()
        }

    }

    private func configureSearchBarOlderVersions() {

        self.searchController = UISearchController.init(searchResultsController: nil)
        self.searchController?.delegate = self
        self.searchController?.searchResultsUpdater = self

        self.definesPresentationContext = true
        self.topView?.backgroundColor = UIApplication.style.navigationBarBackgroundColor

        guard let searchController = self.searchController else { return }
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.autoresizingMask = [.flexibleWidth]
        self.topView?.addSubview(searchController.searchBar)

    }

}

extension SearchableController: UISearchControllerDelegate {

    public func willPresentSearchController(_ searchController: UISearchController) {
        if #available(iOS 11, *) { } else {
            self.topViewConstraint?.constant = 64
        }
    }

    public func didDismissSearchController(_ searchController: UISearchController) {
        if #available(iOS 11, *) { } else {
            self.topViewConstraint?.constant = 44
        }
    }

}

extension SearchableController: UISearchResultsUpdating {

    public func updateSearchResults(for searchController: UISearchController) {

    }

}
