//
//  FilterDetailViewController.swift
//  Movs
//
//  Created by André Salla on 21/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public class FilterDetailViewController: UITableViewController {

    private var viewModel: FilterDetailViewModelProtocol?
    private var coordinator: FilterDetailCoordinatorProtocol?

    public init(viewModel: FilterDetailViewModelProtocol, coordinator: FilterDetailCoordinatorProtocol) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    private func configure() {
        self.title = viewModel?.title
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 1))
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.optionCount ?? 0
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") ??
            UITableViewCell(style: .default, reuseIdentifier: "cell")

        let option = viewModel?.getOption(index: indexPath.row)

        cell.accessoryType = (option?.checked ?? false) ? .checkmark : .none
        cell.textLabel?.text = option?.option

        cell.textLabel?.textColor = UIApplication.style.textColor
        cell.tintColor = UIApplication.style.navigationBarBackgroundColor
        cell.selectionStyle = .none
        return cell
    }

    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.global().async {[weak self] in
            self?.viewModel?.changeOption(at: indexPath.row)
        }
    }

    public override func viewWillDisappear(_ animated: Bool) {
        DispatchQueue.global().async {[weak self] in
            self?.viewModel?.applyChanges()
        }

    }

}

extension FilterDetailViewController: FilterDetailViewModelDelegate {

    public func changedApplied(genres: [Int]) {
        DispatchQueue.main.async {[weak self] in
            self?.coordinator?.complete(genres: genres)
        }
    }

    public func changedApplied(years: [String]) {
        DispatchQueue.main.async {[weak self] in
            self?.coordinator?.complete(year: years)
        }
    }

    public func optionChanged(at index: Int) {
        DispatchQueue.main.async {[weak self] in
            self?.tableView.beginUpdates()
            self?.tableView
                .reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
            self?.tableView.endUpdates()
        }

    }

}
