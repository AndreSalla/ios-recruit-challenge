//
//  MovieDetailViewController.swift
//  Movs
//
//  Created by André Salla on 20/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public class MovieDetailViewController: UIViewController {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!

    private var viewModel: MovieDetailViewModelProtocol?

    public init(viewModel: MovieDetailViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    private func configure() {
        self.title = viewModel?.screenTitle
        self.titleLabel.text = viewModel?.title
        self.yearLabel.text = viewModel?.year
        self.genreLabel.text = viewModel?.genres
        self.descriptionLabel.text = viewModel?.description
        self.posterImageView.setImage(url: Endpoint.imageLargeUrl(path: viewModel?.imagePath ?? ""))
        self.setFavorite(isFavorite: viewModel?.isFavorite ?? false)

    }

    private func setFavorite(isFavorite: Bool) {
        self.favoriteButton.setImage(
            isFavorite ? Images.favoriteOn.image : Images.favoriteOff.image, for: .normal)
    }

    @IBAction func favoritePressed(_ sender: UIControl) {
        DispatchQueue.global().async {[weak self] in
            self?.viewModel?.changeFavorite()
        }
    }

}

extension MovieDetailViewController: MovieDetailViewModelDelegate {

    public func simpleUpdateFavoriteStatus(isFavorite: Bool) {
        DispatchQueue.main.async {[weak self] in
            self?.setFavorite(isFavorite: isFavorite)
        }
    }

    public func updateFavoriteStatus(identifier: Int, isFavorite: Bool) {
        DispatchQueue.main.async {[weak self] in
            self?.setFavorite(isFavorite: isFavorite)
            NotificationCenter.default.post(name: Notification.Name.favoritedNotification,
                                            object: nil,
                                            userInfo: ["id": identifier, "isFavorite": isFavorite])
        }
    }

}
