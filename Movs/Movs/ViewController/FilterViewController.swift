//
//  FilterViewController.swift
//  Movs
//
//  Created by André Salla on 21/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public class FilterViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var applyButton: UIButton!

    private var viewModel: FilterViewModelProtocol?
    private var coordinator: FilterCoordinatorProtocol?

    public init(viewModel: FilterViewModelProtocol, coordinator: FilterCoordinatorProtocol) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    private func configure() {
        self.title = viewModel?.title
        self.hidesBottomBarWhenPushed = true

        applyButton.setTitle(Strings.apply.localized, for: .normal)
        applyButton.backgroundColor = UIApplication.style.navigationBarBackgroundColor
        applyButton.setTitleColor(UIApplication.style.textColor, for: .normal)
        applyButton.titleLabel?.font = UIApplication.style.removeFilterButtomFont
        applyButton.layer.cornerRadius = 5.0
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 1))
    }

    @IBAction public func applyFilterTapped(_ sender: UIControl) {
        coordinator?.applyFilters()
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        coordinator?.dontApplyFilters()
    }

}

extension FilterViewController: UITableViewDelegate, UITableViewDataSource {

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.optionCount ?? 0
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") ??
            UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.textColor = UIApplication.style.textColor
        cell.tintColor = UIApplication.style.textColor
        cell.textLabel?.text = viewModel?.getOption(index: indexPath.row)
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        DispatchQueue.global().async {[weak self] in
            self?.viewModel?.optionSelect(index: indexPath.row)
        }
    }
}

extension FilterViewController: FilterViewModelDelegate {

    public func goToFilterGenre() {
        DispatchQueue.main.async {[weak self] in
            self?.coordinator?.showFilterGenre()
        }
    }

    public func goToFilterYear() {
        DispatchQueue.main.async {[weak self] in
            self?.coordinator?.showFilterYear()
        }
    }

}
