//
//  FavoriteTableViewCell.swift
//  Movs
//
//  Created by André Salla on 21/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public class FavoriteTableViewCell: UITableViewCell {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!

    public var viewModel: FavoritesCellViewModelProtocol? {
        didSet {
            configure()
        }
    }

    private func configure() {
        titleLabel.text = viewModel?.title
        yearLabel.text = viewModel?.year
        overviewLabel.text = viewModel?.overview
        posterImageView.setImage(url: Endpoint.imageUrl(path: viewModel?.posterPath ?? ""))
    }

}
