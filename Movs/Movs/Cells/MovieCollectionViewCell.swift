//
//  MovieCollectionViewCell.swift
//  Movs
//
//  Created by André Salla on 19/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public class MovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var favoriteImage: UIImageView!

    weak var viewModel: MoviesCellViewModelProtocol? {
        didSet {
            titleLabel.text = viewModel?.title
            posterImage.setImage(url: Endpoint.imageUrl(path: viewModel?.posterPath ?? ""))
            favoriteImage.image = viewModel?.favoriteImage.image
        }
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
    }

}
