//
//  FavoritesCellViewModel.swift
//  Movs
//
//  Created by André Salla on 20/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol FavoritesCellViewModelProtocol: class {
    var title: String { get }
    var posterPath: String { get }
    var year: String { get }
    var overview: String { get }
}

public class FavoritesCellViewModel: FavoritesCellViewModelProtocol {

    public var title: String
    public var posterPath: String
    public var year: String
    public var overview: String

    init(movie: Movie) {
        self.title = movie.title
        self.posterPath = movie.posterPath
        self.year = String(movie.releaseDate.split(separator: "-")[0])
        self.overview = movie.overview
    }

}
