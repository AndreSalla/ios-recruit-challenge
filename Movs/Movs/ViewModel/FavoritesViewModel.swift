//
//  FavoritesViewModel.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public enum FavoritesViewState {
    case noData
    case standing
}

public enum FavoritesViewMode {
    case search
    case filter
}

public protocol FavoritesViewModelProtocol: class {
    var delegate: FavoritesViewModelDelegate? { get set }
    var title: String { get }
    var searchTherm: String { get }
    var moviesCount: Int { get }
    init(repository: FavoritesRepositoryProtocol)
    func obtainFavorites()
    func getMovie(at index: Int) -> FavoritesCellViewModelProtocol?
    func searchItem(text: String)
    func changeFavorite(identifier: Int, isFavorite: Bool, update: Bool)
    func unfavorite(at index: Int)
    func removeFilters()
    func showFilters()
    func updateFilters(selectedYears: [String], selectedGenres: [Int])
}

public protocol FavoritesViewModelDelegate: class {
    func stateUpdated(newState: FavoritesViewState)
    func modeUpdated(newState: FavoritesViewMode)
    func filterUpdated()
    func unfavorited(at index: Int, identifier: Int)
    func goToFilters(selectedYear: [String], selectedGenres: [Int])
}

public class FavoritesViewModel: NSObject, FavoritesViewModelProtocol {

    public weak var delegate: FavoritesViewModelDelegate?
    private let repository: FavoritesRepositoryProtocol
    private var moviesList: [Movie]

    private(set) public var searchTherm: String
    private(set) public var yearFilter: [String] {
        didSet {
            mode = (yearFilter.isEmpty && genreFilter.isEmpty) ? .search : .filter
        }
    }
    private(set) public var genreFilter: [Int] {
        didSet {
            mode = (yearFilter.isEmpty && genreFilter.isEmpty) ? .search : .filter
        }
    }

    private var mode: FavoritesViewMode {
        didSet {
            delegate?.modeUpdated(newState: mode)
        }
    }

    private var filteredMoviesList: [Movie]

    public var moviesCount: Int {
        return filteredMoviesList.count
    }

    public var title: String {
        return Strings.titleMovies.localized
    }

    public required init(repository: FavoritesRepositoryProtocol) {
        self.state = .standing
        self.repository = repository
        self.moviesList = []
        self.filteredMoviesList = []
        self.yearFilter = []
        self.genreFilter = []
        self.mode = .search
        self.searchTherm = ""
    }

    private var state: FavoritesViewState {
        didSet {
            delegate?.stateUpdated(newState: state)
        }
    }

    public func obtainFavorites() {
        repository.obtainFavorites()
    }

    public func getMovie(at index: Int) -> FavoritesCellViewModelProtocol? {
        if filteredMoviesList.count > index {
            return FavoritesCellViewModel(movie: filteredMoviesList[index])
        } else {
            return nil
        }
    }

    public func searchItem(text: String) {
        searchTherm = text
        filterMovies()
        delegate?.filterUpdated()
    }

    private func filterMovies() {
        var list: [Movie] = moviesList

        if mode == .filter {
            if !genreFilter.isEmpty {
                list = list.filter {
                    for ids in $0.genreIds {
                        if genreFilter.contains(ids) {
                            return true
                        }
                    }
                    return false }
            }

            if !yearFilter.isEmpty {
                list = list.filter { yearFilter.contains(String($0.releaseDate.split(separator: "-")[0])) }
            }

        }

        list = !searchTherm.isEmpty ?
            list.filter { $0.title.uppercased().contains(
                searchTherm.uppercased()) } : list
        if list.isEmpty && state != .noData {
            state = .noData
        } else if !list.isEmpty && state == .noData {
            state = .standing
        }
        filteredMoviesList = list
    }

    public func changeFavorite(identifier: Int, isFavorite: Bool, update: Bool) {
        if !isFavorite,
            let index = moviesList.index(where: { $0.identifier == identifier }) {
            if repository.remove(movie: moviesList[index]) {
                _ = moviesList.remove(at: index)
                filterMovies()
                if update {
                    delegate?.filterUpdated()
                }
            }
        } else {
            repository.obtainFavorites()
        }

    }

    public func unfavorite(at index: Int) {
        let movie = moviesList[index]
        changeFavorite(identifier: movie.identifier, isFavorite: false, update: false)
        delegate?.unfavorited(at: index, identifier: movie.identifier)
    }

    public func removeFilters() {
        genreFilter = []
        yearFilter = []
        mode = .search
        filterMovies()
        delegate?.filterUpdated()
    }

    public func showFilters() {
        delegate?.goToFilters(selectedYear: yearFilter,
                              selectedGenres: genreFilter)
    }

    public func updateFilters(selectedYears: [String], selectedGenres: [Int]) {
        genreFilter = selectedGenres
        yearFilter = selectedYears
        filterMovies()
        delegate?.filterUpdated()
    }

}

extension FavoritesViewModel: FavoritesRepositoryDelegate {

    public func list(movies: [Movie]) {
        if !movies.isEmpty {
            moviesList = movies
            state = .standing
            filterMovies()
            delegate?.filterUpdated()
        } else {
            state = .noData
        }
    }

}
