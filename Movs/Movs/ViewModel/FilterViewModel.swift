//
//  FilterViewModel.swift
//  Movs
//
//  Created by André Salla on 21/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public enum FilterOptions {
    case year
    case genre

    var description: String {
        switch self {
        case .year: return Strings.year.localized
        case .genre: return Strings.genre.localized
        }
    }
}

public protocol FilterViewModelProtocol: class {

    var delegate: FilterViewModelDelegate? { get set }
    var title: String { get }
    var optionCount: Int { get }

    func getOption(index: Int) -> String
    func optionSelect(index: Int)

}

public protocol FilterViewModelDelegate: class {
    func goToFilterGenre()
    func goToFilterYear()
}

public class FilterViewModel: FilterViewModelProtocol {

    public weak var delegate: FilterViewModelDelegate?
    private(set) public var title: String

    public var optionCount: Int {
        return options.count
    }
    private let options: [FilterOptions]

    public func getOption(index: Int) -> String {
        return options[index].description
    }

    public init() {
        title = Strings.filter.localized
        options = [.year, .genre]
    }

    public func optionSelect(index: Int) {
        switch options[index] {
        case .genre: delegate?.goToFilterGenre()
        case .year: delegate?.goToFilterYear()
        }
    }

}
