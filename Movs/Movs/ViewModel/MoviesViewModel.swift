//
//  MoviesViewModel.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

public enum MoviesViewState {
    case none
    case loading
    case loadingMore
    case error
    case noData
    case standing
    case done
}

public enum MoviesViewMode {
    case view
    case search
}

public protocol MoviesViewModelProtocol: class {
    var delegate: MoviesViewModelDelegate? { get set }
    var title: String { get }
    var searchTherm: String { get }
    var moviesCount: Int { get }
    init(repository: MoviesRepositoryProtocol)
    func getMovie(at index: Int) -> MoviesCellViewModelProtocol?
    func obtainMoreMovies()
    func searchItem(text: String)
    func selectDetail(at index: Int)
    func changeFavorite(identifier: Int, isFavorite: Bool)
}

public protocol MoviesViewModelDelegate: class {
    func stateUpdated(oldState: MoviesViewState, newState: MoviesViewState)
    func filterUpdated()
    func prepareGoNext(info: MovieInfo)
}

public class MoviesViewModel: MoviesViewModelProtocol {

    public weak var delegate: MoviesViewModelDelegate?
    private let repository: MoviesRepositoryProtocol
    private var page: Int
    private var moviesList: [MovieFavorite]
    private var genreList: [Genre]

    private(set) public var searchTherm: String {
        didSet {
            if searchTherm.isEmpty {
                mode = .view
            } else {
                mode = .search
            }
        }
    }

    private var mode: MoviesViewMode

    private var filteredMoviesList: [MovieFavorite]

    public var moviesCount: Int {
        return filteredMoviesList.count
    }

    public var title: String {
        return Strings.titleMovies.localized
    }

    public required init(repository: MoviesRepositoryProtocol) {
        self.state = .none
        self.repository = repository
        self.page = 0
        self.moviesList = []
        self.filteredMoviesList = []
        self.mode = .view
        self.searchTherm = ""
        self.genreList = []
    }

    private var state: MoviesViewState {
        didSet {
            delegate?.stateUpdated(oldState: oldValue, newState: state)
        }
    }

    public func getMovie(at index: Int) -> MoviesCellViewModelProtocol? {
        if filteredMoviesList.count > index {
            return MoviesCellViewModel(movieFavorite: filteredMoviesList[index])
        } else {
            return nil
        }
    }

    public func obtainMoreMovies() {
        guard mode == .view && (state == .none || state == .standing) else {
            return
        }
        state = state == .none ? .loading : .loadingMore
        repository.obtainMoviesList(at: page)
    }

    public func searchItem(text: String) {
        searchTherm = text
        filterMovies()
        delegate?.filterUpdated()
    }

    private func filterMovies() {
        let list = (mode == .search && !searchTherm.isEmpty) ?
            moviesList.filter { $0.movie.title.uppercased().contains(
                searchTherm.uppercased()) } :
        moviesList
        if list.isEmpty && state != .noData {
            state = .noData
        } else if !list.isEmpty && state == .noData {
            state = .standing
        }
        filteredMoviesList = list
    }

    public func selectDetail(at index: Int) {
        let movieFavorite = filteredMoviesList[index]
        let genres = genreList.filter { movieFavorite.movie.genreIds
            .contains($0.identifier) }

        delegate?.prepareGoNext(info: (movie: movieFavorite.movie,
                                       genres: genres,
                                       isFavorite: movieFavorite.isFavorite))
    }

    public func changeFavorite(identifier: Int, isFavorite: Bool) {
        if let index = moviesList.index(where: { $0.movie.identifier == identifier }) {
            let movieFavorite = moviesList.remove(at: index)
            moviesList.insert(MovieFavorite.init(movie: movieFavorite.movie,
                                                 isFavorite: isFavorite), at: index)
            filterMovies()
            delegate?.filterUpdated()

        }
    }

}

extension MoviesViewModel: MoviesRepositoryDelegate {

    public func list(genre: [Genre]) {
        genreList = genre
    }

    public func error(message: String) {
        state = .error
    }

    public func list(movies: Page<Movie>?) {
        if let movies = movies {
            page = movies.page

            checkGenres(list: Array(Set(movies.results.flatMap { $0.genreIds })))

            moviesList.append(contentsOf: movies.results.map {
                MovieFavorite(movie: $0, isFavorite: repository.checkIsFavorite(movie: $0)) })
            state = movies.page == movies.totalPages ? .done : .standing
            filterMovies()
        } else {
            state = .noData
        }
    }

    private func checkGenres(list: [Int]) {
        let allGenreIds = genreList.map { $0.identifier }
        for genreId in list {
            if !allGenreIds.contains(genreId) {
                repository.cleanGenresList()
                repository.obtainGenresList()
                break
            }
        }
    }

}
