//
//  MovieCellViewModel.swift
//  Movs
//
//  Created by André Salla on 20/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol MoviesCellViewModelProtocol: class {
    var title: String { get }
    var posterPath: String { get }
    var favoriteImage: Images { get }
}

public class MoviesCellViewModel: MoviesCellViewModelProtocol {

    public var title: String
    public var posterPath: String
    public var favoriteImage: Images

    init(movieFavorite: MovieFavorite) {
        self.title = movieFavorite.movie.title
        self.posterPath = movieFavorite.movie.posterPath
        self.favoriteImage = movieFavorite.isFavorite ?
            Images.favoriteOn : Images.favoriteOff
    }

}
