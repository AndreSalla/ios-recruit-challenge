//
//  FilterViewModel.swift
//  Movs
//
//  Created by André Salla on 21/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol FilterDetailViewModelProtocol: class {

    var delegate: FilterDetailViewModelDelegate? { get set }
    var title: String { get }
    var optionCount: Int { get }

    func getOption(index: Int) -> OptionSelect
    func changeOption(at index: Int)
    func applyChanges()
}

public protocol FilterDetailViewModelDelegate: class {
    func optionChanged(at index: Int)
    func changedApplied(genres: [Int])
    func changedApplied(years: [String])
}

public typealias OptionSelect = (option: String, checked: Bool)

public class FilterDetailViewModel: FilterDetailViewModelProtocol {

    public weak var delegate: FilterDetailViewModelDelegate?
    private var repository: FilterDetailRepositoryProtocol

    public var title: String {
        return mode.description
    }

    private var selectedYears: [String]
    private var selectedGenres: [Int]

    private var availableYears: [String]
    private var availableGenres: [Genre]

    public var optionCount: Int {
        return options.count
    }
    private var options: [OptionSelect]
    private let mode: FilterOptions

    public func getOption(index: Int) -> OptionSelect {
        return options[index]
    }

    public init(repository: FilterDetailRepositoryProtocol, selectedYears: [String]) {
        mode = .year
        self.repository = repository
        self.selectedYears = selectedYears

        let years = repository.listFavorites()
            .map { String($0.releaseDate.split(separator: "-")[0]) }

        self.availableYears = Array(Set(years))

        self.availableGenres = []
        self.selectedGenres = []
        self.options = []
        self.populateOptions()
    }

    public init(repository: FilterDetailRepositoryProtocol, selectedGenres: [Int]) {
        mode = .genre
        self.repository = repository
        self.selectedYears = []
        self.selectedGenres = selectedGenres
        self.availableYears = []
        self.availableGenres = repository.listGenres()
        self.options = []
        self.populateOptions()
    }

    public func changeOption(at index: Int) {
        let option = options.remove(at: index)
        options.insert((option: option.option, checked: !option.checked), at: index)
        delegate?.optionChanged(at: index)
    }

    private func populateOptions() {
        if mode == .genre {
            options = availableGenres.map {
                (option: $0.description,
                 checked: selectedGenres.contains($0.identifier)) }
        } else {
            var preOptions = availableYears
            selectedYears.forEach { year in
                if !preOptions.contains(year) {
                    preOptions.append(year)
                }
            }
            options = preOptions.sorted().map {
                (option: $0, checked: selectedYears.contains($0)) }
        }
    }

    public func applyChanges() {
        if mode == .genre {
            var genreCompleteOption: [Int] = []
            for index in 0 ... options.count - 1 where options[index].checked {
                genreCompleteOption.append(availableGenres[index].identifier)
            }
            delegate?.changedApplied(genres: genreCompleteOption)
        } else {
            delegate?.changedApplied(years:
                options.compactMap { $0.checked ? $0.option : nil })
        }
    }

}
