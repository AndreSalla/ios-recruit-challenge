//
//  MovieDetailViewModel.swift
//  Movs
//
//  Created by André Salla on 20/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public typealias MovieInfo = (movie: Movie, genres: [Genre], isFavorite: Bool)

public protocol MovieDetailViewModelProtocol: class {

    var delegate: MovieDetailViewModelDelegate? { get set }
    init(movie: Movie, genres: [Genre], isFavorite: Bool, repository: MovieDetailRepositoryProtocol)

    var imagePath: String { get }
    var title: String { get }
    var year: String { get }
    var genres: String { get }
    var description: String { get }
    var isFavorite: Bool { get }

    var screenTitle: String { get }
    func changeFavorite()
}

public protocol MovieDetailViewModelDelegate: class {
    func updateFavoriteStatus(identifier: Int, isFavorite: Bool)
    func simpleUpdateFavoriteStatus(isFavorite: Bool)
}

public class MovieDetailViewModel: MovieDetailViewModelProtocol {

    private let movie: Movie
    private let genresList: [Genre]
    private(set) public var isFavorite: Bool
    public weak var delegate: MovieDetailViewModelDelegate?
    private let repository: MovieDetailRepositoryProtocol

    public var title: String {
        return movie.title
    }

    public var year: String {
        return String(movie.releaseDate.split(separator: "-")[0])
    }

    public var genres: String {
        return genresList
            .map { $0.description }
            .reduce("", { $0.isEmpty ? "\($1)" : "\($0), \($1)" })
    }

    public var description: String {
        return movie.overview
    }

    public var imagePath: String {
        return movie.posterPath
    }

    public var screenTitle: String {
        return Strings.titleMoviesDetail.localized
    }

    public required init(movie: Movie, genres: [Genre], isFavorite: Bool,
                         repository: MovieDetailRepositoryProtocol) {
        self.movie = movie
        self.genresList = genres
        self.repository = repository
        self.isFavorite = isFavorite
    }

    public func changeFavorite() {
        repository.changeFavorite(movie: movie, isFavorite: !isFavorite)
    }

}

extension MovieDetailViewModel: MovieDetailRepositoryDelegate {

    public func favoriteChanged() {
        isFavorite = !isFavorite
        delegate?.updateFavoriteStatus(identifier: movie.identifier, isFavorite: isFavorite)
    }

    public func errorChangingFavorite() {
        delegate?.updateFavoriteStatus(identifier: movie.identifier, isFavorite: isFavorite)
    }

}
