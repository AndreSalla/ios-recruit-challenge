//
//  FavoritesCoordinator.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol FavoritesCoordinatorProtocol: class {
    func showFilters(selectedYears: [String], selectedGenres: [Int])
    func updateFilters(selectedYears: [String], selectedGenres: [Int])
}

public class FavoritesCoordinator: Coordinator, FavoritesCoordinatorProtocol {

    private weak var tabController: UITabBarController?
    private weak var navigation: UINavigationController?
    private weak var controller: FavoritesViewControllerProtocol?

    init(tabController: UITabBarController) {
        self.tabController = tabController
    }

    override func start() {
        let repository = FavoritesRepository()
        let viewModel = FavoritesViewModel(repository: repository)

        let favoritesViewController = FavoritesViewController(viewModel: viewModel, coordinator: self)

        repository.delegate = viewModel
        viewModel.delegate = favoritesViewController

        let navigation = UINavigationController.init(rootViewController: favoritesViewController)

        var arrayControllers = self.tabController?.viewControllers ?? []
        arrayControllers.append(navigation)

        self.controller = favoritesViewController
        self.tabController?.viewControllers = arrayControllers
        self.navigation = navigation
    }

    public func showFilters(selectedYears: [String], selectedGenres: [Int]) {
        if let navigation = navigation {
            let filterCoordinator = FilterCoordinator(
                navigation: navigation, master: self,
                selectedYears: selectedYears, selectedGenres: selectedGenres)
            filterCoordinator.start()
            addChild(coordinator: filterCoordinator)
        }
    }

    public func updateFilters(selectedYears: [String], selectedGenres: [Int]) {
        controller?.updateFilters(selectedYears: selectedYears,
                                  selectedGenres: selectedGenres)
    }
}
