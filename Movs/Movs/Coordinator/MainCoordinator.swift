//
//  MainCoordinator.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public class MainCoordinator: Coordinator {

    let window: UIWindow?

    init(window: UIWindow?) {
        self.window = window
    }

    override func start() {
        let viewController: UITabBarController = UITabBarController.init()

        let moviesCoordinator = MoviesCoordinator(tabController: viewController)
        self.addChild(coordinator: moviesCoordinator)
        moviesCoordinator.start()

        let favoritesCoordinator = FavoritesCoordinator(tabController: viewController)
        self.addChild(coordinator: favoritesCoordinator)
        favoritesCoordinator.start()

        window?.rootViewController = viewController
        window?.makeKeyAndVisible()

    }

}
