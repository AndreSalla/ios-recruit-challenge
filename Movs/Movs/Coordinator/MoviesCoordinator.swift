//
//  MoviesCoordinator.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol MoviesCoordinatorProtocol: class {

    func goDetail(movie: Movie, genres: [Genre], isFavorite: Bool)

}

public class MoviesCoordinator: Coordinator {

    private weak var tabController: UITabBarController?
    private weak var navigation: UINavigationController?

    init(tabController: UITabBarController) {
        self.tabController = tabController
    }

    override func start() {
        let repository = MoviesRepository()
        let viewModel = MoviesViewModel(repository: repository)
        let movieViewController = MoviesViewController(viewModel: viewModel, coordinator: self)

        repository.delegate = viewModel
        viewModel.delegate = movieViewController
        let navigation = UINavigationController.init(rootViewController: movieViewController)

        var arrayControllers = self.tabController?.viewControllers ?? []
        arrayControllers.append(navigation)
        self.tabController?.viewControllers = arrayControllers
        self.navigation = navigation
    }

}

extension MoviesCoordinator: MoviesCoordinatorProtocol {

    public func goDetail(movie: Movie, genres: [Genre], isFavorite: Bool) {
        let coordinator = MovieDetailCoordinator(navigationController: navigation,
                                                 movie: movie,
                                                 genres: genres,
                                                 isFavorite: isFavorite,
                                                 master: self)
        self.addChild(coordinator: coordinator)
        coordinator.start()
    }

}
