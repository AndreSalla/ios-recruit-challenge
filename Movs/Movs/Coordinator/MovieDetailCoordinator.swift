//
//  MovieDetailCoordinator.swift
//  Movs
//
//  Created by André Salla on 20/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public class MovieDetailCoordinator: Coordinator {

    private weak var navigationController: UINavigationController?
    private let movie: Movie
    private let genres: [Genre]
    private let isFavorite: Bool
    private let master: Coordinator

    public required init(navigationController: UINavigationController?,
                         movie: Movie, genres: [Genre], isFavorite: Bool,
                         master: Coordinator) {
        self.navigationController = navigationController
        self.movie = movie
        self.genres = genres
        self.isFavorite = isFavorite
        self.master = master
    }

    override func start() {
        let repository = MovieDetailRepository()
        let viewModel = MovieDetailViewModel(
            movie: movie, genres: genres, isFavorite: isFavorite, repository: repository)
        let viewController = MovieDetailViewController(viewModel: viewModel)

        repository.delegate = viewModel
        viewModel.delegate = viewController

        self.navigationController?.pushViewController(viewController, animated: true)
    }

    override func stop() {
        self.navigationController?.popViewController(animated: true)
        master.removeChild(coordinator: self)
    }

}
