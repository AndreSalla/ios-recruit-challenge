//
//  FilterCoordinator.swift
//  Movs
//
//  Created by André Salla on 21/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol FilterCoordinatorProtocol: class {
    func showFilterYear()
    func showFilterGenre()
    func updateFilter(years: [String])
    func updateFilter(genres: [Int])
    func applyFilters()
    func dontApplyFilters()
}

public class FilterCoordinator: Coordinator, FilterCoordinatorProtocol {

    private weak var navigation: UINavigationController?
    private weak var master: (FavoritesCoordinatorProtocol & Coordinator)?
    private var selectedYears: [String]
    private var selectedGenres: [Int]

    init(navigation: UINavigationController, master: (FavoritesCoordinatorProtocol & Coordinator),
         selectedYears: [String], selectedGenres: [Int]) {
        self.navigation = navigation
        self.master = master
        self.selectedYears = selectedYears
        self.selectedGenres = selectedGenres
    }

    override func start() {
        let viewModel = FilterViewModel()

        let filterViewController = FilterViewController(viewModel: viewModel, coordinator: self)

        viewModel.delegate = filterViewController

        navigation?.pushViewController(filterViewController, animated: true)
    }

    override func stop() {
        navigation?.popViewController(animated: true)
    }

    public func showFilterGenre() {
        if let navigation = navigation {
            let coordinator = FilterDetailCoordinator(
                navigation: navigation, master: self,
                selectedGenres: selectedGenres)
            addChild(coordinator: coordinator)
            coordinator.start()
        }
    }

    public func showFilterYear() {
        if let navigation = navigation {
            let coordinator = FilterDetailCoordinator(
                navigation: navigation, master: self,
                selectedYears: selectedYears)
            addChild(coordinator: coordinator)
            coordinator.start()
        }
    }

    public func updateFilter(years: [String]) {
        selectedYears = years
    }

    public func updateFilter(genres: [Int]) {
        selectedGenres = genres
    }

    public func applyFilters() {
        master?.updateFilters(selectedYears: selectedYears,
                              selectedGenres: selectedGenres)
        master?.removeChild(coordinator: self)
        stop()
    }

    public func dontApplyFilters() {
        master?.removeChild(coordinator: self)
    }

}
