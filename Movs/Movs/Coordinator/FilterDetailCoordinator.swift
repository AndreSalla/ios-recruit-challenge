//
//  FilterDetailCoordinator.swift
//  Movs
//
//  Created by André Salla on 21/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol FilterDetailCoordinatorProtocol: class {
    func complete(genres: [Int])
    func complete(year: [String])
}

public class FilterDetailCoordinator: Coordinator, FilterDetailCoordinatorProtocol {

    private weak var navigation: UINavigationController?
    private weak var master: (FilterCoordinatorProtocol & Coordinator)?
    private let selectedYears: [String]
    private let selectedGenres: [Int]
    private let mode: FilterOptions

    public init(navigation: UINavigationController, master: (FilterCoordinatorProtocol & Coordinator),
                selectedYears: [String]) {
        self.navigation = navigation
        self.master = master
        self.selectedYears = selectedYears
        self.selectedGenres = []
        self.mode = .year
    }

    public init(navigation: UINavigationController, master: (FilterCoordinatorProtocol & Coordinator),
                selectedGenres: [Int]) {
        self.navigation = navigation
        self.master = master
        self.selectedYears = []
        self.selectedGenres = selectedGenres
        self.mode = .genre
    }

    public override func start() {
        let repository = FilterDetailRepository()
        let viewModel = mode == .genre ?
            FilterDetailViewModel.init(repository: repository, selectedGenres: selectedGenres) :
            FilterDetailViewModel.init(repository: repository, selectedYears: selectedYears)

        let filterDetailViewController = FilterDetailViewController(viewModel: viewModel, coordinator: self)

        viewModel.delegate = filterDetailViewController

        navigation?.pushViewController(filterDetailViewController, animated: true)
    }

    public override func stop() {
        master?.removeChild(coordinator: self)
    }

    public func complete(genres: [Int]) {
        master?.updateFilter(genres: genres)
        stop()
    }

    public func complete(year: [String]) {
        master?.updateFilter(years: year)
        stop()
    }

}
