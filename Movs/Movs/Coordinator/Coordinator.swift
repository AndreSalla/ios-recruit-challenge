//
//  Coordinator.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public class Coordinator {

    private(set) var childCoordinators: [Coordinator] = []

    func start() {
        preconditionFailure("Esse método precisa ser sobrescrito para ser utilizado")
    }

    func stop() {
        preconditionFailure("Esse método precisa ser sobrescrito para ser utilizado")
    }

    func addChild(coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }

    func removeChild(coordinator: Coordinator) {
        if let index = childCoordinators.index(of: coordinator) {
            childCoordinators.remove(at: index)
        }
    }

}

extension Coordinator: Equatable {

    public static func == (lhs: Coordinator, rhs: Coordinator) -> Bool {
        //Não há um ID, então a checagem será feita por endereço de memória
        return lhs === rhs
    }

}
