//
//  Genre.swift
//  Movs
//
//  Created by André Salla on 19/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public struct GenresList: Codable {
    let genres: [Genre]
}

public struct Genre: Codable {

    let identifier: Int
    let description: String

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case description = "name"
    }

}
