//
//  Page.swift
//  Movs
//
//  Created by André Salla on 19/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public struct Page<T: Codable>: Codable {

    let page: Int
    let totalResults: Int
    let totalPages: Int
    let results: [T]

    enum CodingKeys: String, CodingKey {
        case page = "page"
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results = "results"
    }

}
