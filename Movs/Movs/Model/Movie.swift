//
//  Movie.swift
//  Movs
//
//  Created by André Salla on 19/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public struct Movie: Codable {

    let identifier: Int
    let title: String
    let posterPath: String
    let overview: String
    let genreIds: [Int]
    let releaseDate: String

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case title = "title"
        case posterPath = "poster_path"
        case overview = "overview"
        case genreIds = "genre_ids"
        case releaseDate = "release_date"
    }

}

public struct MovieFavorite {
    let movie: Movie
    let isFavorite: Bool
}
