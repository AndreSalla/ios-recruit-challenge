//
//  CustomNavigationBar.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

extension UINavigationBar {

    @available(*, deprecated: 11, message: "Should use navigationItem searchController")
    func hideHairlineForSearch() {
        self.setBackgroundImage(UIImage.from(color: self.barTintColor), for: UIBarMetrics.default)
        self.shadowImage = UIImage.from(color: .clear)
    }

    @available(*, deprecated: 11, message: "Should use navigationItem searchController")
    func showHairlineForSearch() {
        self.setBackgroundImage(UIImage.from(color: self.barTintColor), for: UIBarMetrics.default)
        self.shadowImage = nil
    }

}

extension UIImage {
    static func from(color: UIColor?) -> UIImage? {
        guard let color = color else {
            return nil
        }

        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}
