//
//  MovsStyle.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//
import UIKit

public final class MovsStyle: Style {

    public static let instance = MovsStyle()

    public var navigationBarBackgroundColor: UIColor { return .creamCan }

    public var navigationBarTitleColor: UIColor { return .bunting }

    public var tabBarBackgroundColor: UIColor { return .creamCan }

    public var tabBarSelectedColor: UIColor { return .bunting }

    public var removeFilterButtonColor: UIColor { return .bunting }

    public var tableCellBackgroundColor: UIColor { return .iron }

    public var collectionCellBackgroundColor: UIColor { return .bunting }

    public var searchBarBackgroundColor: UIColor { return .creamCan }

    public var removeFilterTextColor: UIColor { return .creamCan }

    public var navigationTitleFont: UIFont { return UIFont.systemFont(ofSize: 17) }

    public var textColor: UIColor { return .bunting }

    public var removeFilterButtomFont: UIFont { return UIFont.boldSystemFont(ofSize: 17) }

}

extension UIColor {

    static var creamCan: UIColor { return UIColor(red: 0.97, green: 0.81, blue: 0.36, alpha: 1.0) }
    static var butterCup: UIColor { return UIColor(red: 0.85, green: 0.59, blue: 0.12, alpha: 1.0) }
    static var bunting: UIColor { return UIColor(red: 0.18, green: 0.19, blue: 0.28, alpha: 1.0) }
    static var iron: UIColor { return UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0) }

}
