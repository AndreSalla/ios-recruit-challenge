//
//  Images.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit
import Kingfisher

public enum Images {
    case tabBarItemMovies
    case tabBarItemFavorites
    case favoriteOn
    case favoriteOff
    case filter
}

extension Images {

    var image: UIImage? {
        switch self {
        case .tabBarItemMovies: return get("list_icon")
        case .tabBarItemFavorites: return get("favorite_empty_icon")
        case .favoriteOn: return get("favorite_full_icon")
        case .favoriteOff: return get("favorite_gray_icon")
        case .filter: return get("FilterIcon")
        }
    }

    private func get(_ name: String) -> UIImage? {
        return UIImage(named: name)
    }

}

extension UIImageView {

    func setImage(url: URL?) {
        self.kf.setImage(with: url)
    }

}
