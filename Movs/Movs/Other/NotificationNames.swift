//
//  NotificationNames.swift
//  Movs
//
//  Created by André Salla on 19/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

extension Notification.Name {

    static var favoritedNotification: Notification.Name {
        return Notification.Name("nFavorite")
    }

}
