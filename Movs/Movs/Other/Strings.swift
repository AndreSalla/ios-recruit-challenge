//
//  Strings.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//
import UIKit

enum Strings {

    case titleMovies
    case titleMoviesDetail
    case titleFavorites
    case unknownError
    case noData
    case unfavorite
    case removeFilter
    case year
    case genre
    case filter
    case apply
}

extension Strings {

    var localized: String {
        switch self {
        case .titleMoviesDetail: return NSLocalizedString("title-movies-detail",
                                                         comment: "Text for Navigation Title Movie")
        case .titleMovies: return NSLocalizedString("title-movies",
                                                      comment: "Text for Tab Title Movies")
        case .titleFavorites: return NSLocalizedString("title-favorites",
                                                      comment: "Text for Tab Title Favorites")
        case .unknownError: return NSLocalizedString("unknown-error",
                                                     comment: "Text for an unexpected error")
        case .noData: return NSLocalizedString("no-movies-found",
                                               comment: "Text to be shown when no movies are found")
        case .unfavorite: return NSLocalizedString("unfavorite",
                                                  comment: "Text to unfavorite movies")
        case .removeFilter: return NSLocalizedString("remove-filter",
                                                      comment: "Text to remove filter buttom")
        case .year: return NSLocalizedString("year",
                                             comment: "Text to filter Date")
        case .genre: return NSLocalizedString("genre",
                                             comment: "Text to filter Genre")
        case .filter: return NSLocalizedString("filter",
                                             comment: "Text to Filter Screen Title")
        case .apply: return NSLocalizedString("apply",
                                              comment: "Text Apply Buttom")
        }
    }

}
