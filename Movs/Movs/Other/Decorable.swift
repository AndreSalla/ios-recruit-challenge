//
//  Decorable.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol Decorable {

    func decorate(style: Style)

}

public protocol Style {

    // MARK: Color

    var navigationBarBackgroundColor: UIColor { get }
    var navigationBarTitleColor: UIColor { get }
    var tabBarBackgroundColor: UIColor { get }
    var tabBarSelectedColor: UIColor { get }
    var removeFilterButtonColor: UIColor { get }
    var removeFilterTextColor: UIColor { get }
    var tableCellBackgroundColor: UIColor { get }
    var collectionCellBackgroundColor: UIColor { get }
    var searchBarBackgroundColor: UIColor { get }
    var textColor: UIColor { get }

    // MARK: Font

    var navigationTitleFont: UIFont { get }
    var removeFilterButtomFont: UIFont { get }

}

extension AppDelegate: Decorable {

    func decorate(style: Style) {

        let navigationBarAppear = UINavigationBar.appearance()
        navigationBarAppear.tintColor = style.navigationBarTitleColor
        navigationBarAppear.barTintColor = style.navigationBarBackgroundColor
        navigationBarAppear.isTranslucent = false
        navigationBarAppear.titleTextAttributes = [.font: style.navigationTitleFont,
                                                   .foregroundColor: style.navigationBarTitleColor]

        let toolBarAppear = UITabBar.appearance()
        toolBarAppear.isTranslucent = false
        toolBarAppear.barTintColor = style.navigationBarBackgroundColor

        let tabBarAppear = UITabBar.appearance()
        tabBarAppear.isTranslucent = false
        tabBarAppear.tintColor = style.tabBarSelectedColor
        tabBarAppear.barTintColor = style.tabBarBackgroundColor

        let collectionItemAppear = UICollectionViewCell.appearance()
        collectionItemAppear.backgroundColor = style.collectionCellBackgroundColor

        let searchBarAppear = UISearchBar.appearance()
        searchBarAppear.barTintColor = style.searchBarBackgroundColor

    }

}
