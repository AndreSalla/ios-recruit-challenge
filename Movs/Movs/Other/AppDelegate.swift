//
//  AppDelegate.swift
//  Movs
//
//  Created by André Salla on 18/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: Coordinator?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        self.decorate(style: UIApplication.style)

        self.window = UIWindow(frame: UIScreen.main.bounds)

        self.coordinator = MainCoordinator(window: window)
        self.coordinator?.start()

        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        _ = DataManager.saveContext()
    }

}

extension UIApplication {

    //Para alterar o estilo do app, é necessário mudar o retorno da propriedade computada
    static var style: Style { return MovsStyle.instance }

}
