//
//  Endpoint.swift
//  Movs
//
//  Created by André Salla on 19/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public enum Endpoint {

    case listMovie
    case listMovieGenre

}

extension Endpoint {

    private static let server = "https://api.themoviedb.org/3"
    private static let imageServer = "https://image.tmdb.org/t/p/w300_and_h450_bestv2"
    private static let imageLargeServer = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"
    static let apiKey = "e0d2d406ef31f345e419e7ad50574b38"

    private var address: String {
        switch self {
        case .listMovie: return "/movie/popular"
        case .listMovieGenre: return "/genre/movie/list"
        }
    }

    private var fullAddress: String {
        return "\(Endpoint.server)\(self.address)"
    }

    var url: URL? {
        return URL(string: fullAddress)
    }

    static func imageUrl(path: String) -> URL? {
        return URL(string: "\(Endpoint.imageServer)\(path)")
    }

    static func imageLargeUrl(path: String) -> URL? {
        return URL(string: "\(Endpoint.imageLargeServer)\(path)")
    }

}
