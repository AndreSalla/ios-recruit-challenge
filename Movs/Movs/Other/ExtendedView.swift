//
//  ExtendedView.swift
//  Movs
//
//  Created by André Salla on 19/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

class ExtendedView: UIView {

    override func willMove(toWindow newWindow: UIWindow?) {
        self.layer.shadowOffset = CGSize(width: 0, height: 1.0/UIScreen.main.scale)
        self.layer.shadowRadius = 0
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.25
    }

}
