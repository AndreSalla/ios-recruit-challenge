//
//  ApiCall.swift
//  Movs
//
//  Created by André Salla on 19/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public enum Result<T: Codable> {
    case sucess(object: T?)
    case error(message: String)
}

public class ApiCall<T: Codable>: NSObject {

    public static func get(endPoint: Endpoint,
                           parameters: [String: String]? = nil,
                           completion: @escaping (Result<T>) -> Void) {
        if let url = endPoint.url {
            var request = URLComponents(url: url, resolvingAgainstBaseURL: true)
            request?.queryItems = [
                URLQueryItem(name: "api_key", value: Endpoint.apiKey),
                URLQueryItem(name: "language", value:
                    Locale.current.identifier.replacingOccurrences(of: "_", with: "-"))
            ]
            if let parameters = parameters {
                request?.queryItems?.append(contentsOf:
                    parameters.map { URLQueryItem(name: $0.key, value: $0.value) })
            }

            if let urlParam = request?.url {

                let task = URLSession.shared.dataTask(with: urlParam) { (data, _, errorReturn) in
                    if let error = errorReturn {
                        completion(Result<T>.error(message: error.localizedDescription))
                    } else if let data = data {
                        completion(
                            Result<T>.sucess(object: try? JSONDecoder.init().decode(T.self, from: data)))
                    } else {
                        completion(Result<T>.sucess(object: nil))
                    }
                }
                task.resume()
            }
        }
    }

}
