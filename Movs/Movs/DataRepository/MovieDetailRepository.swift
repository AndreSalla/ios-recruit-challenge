//
//  MovieDetailRepository.swift
//  Movs
//
//  Created by André Salla on 20/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol MovieDetailRepositoryProtocol: class {
    var delegate: MovieDetailRepositoryDelegate? { get set }
    func changeFavorite(movie: Movie, isFavorite: Bool)
}

public protocol MovieDetailRepositoryDelegate: class {
    func favoriteChanged()
    func errorChangingFavorite()
}

public class MovieDetailRepository: MovieDetailRepositoryProtocol {

    public weak var delegate: MovieDetailRepositoryDelegate?

    public func changeFavorite(movie: Movie, isFavorite: Bool) {
        if (isFavorite) ? MoviesData.save(movie: movie) :
            MoviesData.remove(identifier: movie.identifier) {
            delegate?.favoriteChanged()
        } else {
            delegate?.errorChangingFavorite()
        }
    }

}
