//
//  MoviesRepository.swift
//  Movs
//
//  Created by André Salla on 19/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol MoviesRepositoryProtocol: class {

    var delegate: MoviesRepositoryDelegate? { get set }
    func obtainMoviesList(at page: Int)
    func obtainGenresList()
    func cleanGenresList()
    func checkIsFavorite(movie: Movie) -> Bool

}

public protocol MoviesRepositoryDelegate: class {
    func error(message: String)
    func list(movies: Page<Movie>?)
    func list(genre: [Genre])
}

public class MoviesRepository: NSObject, MoviesRepositoryProtocol {

    public weak var delegate: MoviesRepositoryDelegate?
    private let semaphore = DispatchSemaphore(value: 1)

    public func obtainMoviesList(at page: Int) {
        ApiCall<Page<Movie>>.get(endPoint: .listMovie, parameters: ["page": "\(page + 1)"]) {[weak self] (result) in
            switch result {
            case .sucess(let object): self?.delegate?.list(movies: object)
            case .error(let message): self?.delegate?.error(message: message)
            }
        }
    }

    public func cleanGenresList() {
        _ = GenresData.removeAll()
    }

    public func obtainGenresList() {
        semaphore.wait()

        let genres = GenresData.getAll()

        if genres.isEmpty {
            ApiCall<GenresList>.get(endPoint: .listMovieGenre) {[weak self] (result) in
                switch result {
                case .sucess(let object):
                    if let object = object {
                        object.genres.forEach { _ = GenresData.save(genre: $0) }
                    }
                    self?.semaphore.signal()
                    self?.delegate?.list(genre: object?.genres ?? [])
                case .error(let message):
                    self?.delegate?.error(message: message)
                    self?.semaphore.signal()
                }
            }
        } else {
            self.delegate?.list(genre: genres)
            self.semaphore.signal()
        }
    }

    public func checkIsFavorite(movie: Movie) -> Bool {
        return MoviesData.exists(identifier: movie.identifier)
    }

}
