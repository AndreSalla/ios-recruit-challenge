//
//  FilterDetailRepository.swift
//  Movs
//
//  Created by André Salla on 21/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol FilterDetailRepositoryProtocol: class {
    func listFavorites() -> [Movie]
    func listGenres() -> [Genre]
}

public class FilterDetailRepository: FilterDetailRepositoryProtocol {

    public func listFavorites() -> [Movie] {
        return MoviesData.getAll()
    }

    public func listGenres() -> [Genre] {
        return GenresData.getAll()
    }

}
