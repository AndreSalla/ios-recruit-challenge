//
//  FavoritesRepository.swift
//  Movs
//
//  Created by André Salla on 20/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit

public protocol FavoritesRepositoryProtocol: class {
    var delegate: FavoritesRepositoryDelegate? { get set }
    func obtainFavorites()
    func remove(movie: Movie) -> Bool
}

public protocol FavoritesRepositoryDelegate: class {
    func list(movies: [Movie])
}

public class FavoritesRepository: FavoritesRepositoryProtocol {

    public weak var delegate: FavoritesRepositoryDelegate?

    public func remove(movie: Movie) -> Bool {
        return MoviesData.remove(identifier: movie.identifier)
    }

    public func obtainFavorites() {
        let movies = MoviesData.getAll()
        delegate?.list(movies: movies)
    }

}
