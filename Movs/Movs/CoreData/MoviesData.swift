//
//  MoviesData.swift
//  Movs
//
//  Created by André Salla on 20/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit
import CoreData

public final class MoviesData {

    public static func getAll() -> [Movie] {

        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieEntity")
        let sortDesc = NSSortDescriptor(key: "title", ascending: true)
        request.sortDescriptors = [sortDesc]

        var entities: [MovieEntity]? = nil

        let data = try? DataManager.managedObjectContext.fetch(request)
        entities = data?.compactMap { $0 as? MovieEntity }
        return (entities ?? [])
            .map { Movie.init(
                identifier: Int($0.identifier),
                            title: $0.title ?? "",
                            posterPath: $0.posterPath ?? "",
                            overview: $0.overview ?? "",
                            genreIds: $0.genres?.compactMap { ($0 as? GenreEntity)?.identifier }.map { Int($0) } ?? [],
                            releaseDate: $0.year ?? "") }
    }

    static func remove(identifier: Int) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieEntity")
        let predicate = NSPredicate(format: "identifier = %d", identifier)
        request.predicate = predicate
        let data = try? DataManager.managedObjectContext.fetch(request)
        data?.compactMap { $0 as? MovieEntity }
            .forEach { DataManager.managedObjectContext.delete($0) }
        return DataManager.saveContext()
    }

    static func save(movie: Movie) -> Bool {

        var movieEntity: MovieEntity? = nil
        if #available(iOS 10.0, *) {
            movieEntity = MovieEntity(context: DataManager.managedObjectContext)
        } else {
            if let entityDesc = NSEntityDescription.entity(forEntityName:
                "MovieEntity", in: DataManager.managedObjectContext) {
                movieEntity = MovieEntity(entity: entityDesc, insertInto: DataManager.managedObjectContext)
            }
        }
        movieEntity?.identifier = Int64(movie.identifier)
        movieEntity?.title = movie.title
        movieEntity?.posterPath = movie.posterPath
        movieEntity?.overview = movie.overview
        movieEntity?.year = movie.releaseDate

        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "GenreEntity")
        let predicate = NSPredicate(format: "identifier IN %@", movie.genreIds)
        request.predicate = predicate
        let data = try? DataManager.managedObjectContext.fetch(request)
        let genreEntity = data?.compactMap { $0 as? GenreEntity } ?? []

        movieEntity?.addToGenres(NSSet(array: genreEntity))

        return DataManager.saveContext()

    }

    static func exists(identifier: Int) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieEntity")
        let predicate = NSPredicate(format: "identifier = %d", identifier)
        request.predicate = predicate
        let data = try? DataManager.managedObjectContext.fetch(request)
        return !(data?.compactMap { $0 as? MovieEntity } ?? []).isEmpty
    }

}
