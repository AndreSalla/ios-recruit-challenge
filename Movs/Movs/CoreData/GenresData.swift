//
//  GenresData.swift
//  Movs
//
//  Created by André Salla on 20/06/18.
//  Copyright © 2018 Andre Salla. All rights reserved.
//

import UIKit
import CoreData

public final class GenresData {

    public static func getAll() -> [Genre] {

        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "GenreEntity")
        let sortDesc = NSSortDescriptor(key: "name", ascending: true)
        request.sortDescriptors = [sortDesc]

        var entities: [GenreEntity]? = nil

        let data = try? DataManager.managedObjectContext.fetch(request)
        entities = data?.compactMap { $0 as? GenreEntity }

        return (entities ?? [])
            .map { Genre(identifier: Int($0.identifier), description: $0.name ?? "") }
    }

    static func removeAll() -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "GenreEntity")
        let data = try? DataManager.managedObjectContext.fetch(request)
        data?.compactMap { $0 as? GenreEntity }
            .forEach { DataManager.managedObjectContext.delete($0) }
        return DataManager.saveContext()
    }

    static func save(genre: Genre) -> Bool {

        var genreEntity: GenreEntity? = nil
        if #available(iOS 10.0, *) {
            genreEntity = GenreEntity(context: DataManager.managedObjectContext)
        } else {
            if let entityDesc = NSEntityDescription.entity(
                forEntityName: "GenreEntity", in: DataManager.managedObjectContext) {
                genreEntity = GenreEntity(entity: entityDesc, insertInto: DataManager.managedObjectContext)
            }
        }
        genreEntity?.identifier = Int64(genre.identifier)
        genreEntity?.name = genre.description
        genreEntity?.movies = nil
        return DataManager.saveContext()

    }

}
